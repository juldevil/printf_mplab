/* 
 * File:   main.c
 * Author: Julie
 *
 * Created on 14 de octubre de 2020, 10:29 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "fuses.h"
#include "UART.h"

/*
 * Printing to the UART Console in MPLAB X IDE Simulator
 */

#define _XTAL_FREQ 20000000 // fr 20MHz

void main()
{
    init_uart();
    
    while(1)
    {
        printf("Hello friends\n");
        __delay_ms(1000);
    }
}
